

# PacketizedEnergy

This is a single-page web app developed using ***Node.JS*** & ***React.JS*** for demonstration and learning purposes. The app uses ***D3*** to visualize a time-series of electricity prices. Electricity prices are typically expressed in $/MWh and vary in space and time. All Locational Marginal Prices(LMPs) data were taken from this web site ■ [ISO NEW ENGLAND](https://www.iso-ne.com/isoexpress/web/charts/guest-hub?p_p_id=fiveminuterealtimelmp_WAR_isoneportlet_INSTANCE_a3zQ&p_p_lifecycle=0&p_p_state=normal&p_p_state_rcv=1).



### Installing Dependencies
Before running any of the instructions below make sure that you have Node.JS installed on your system. <br>
- Run `npm -v` to check, or install Node.JS from this link [npm](https://nodejs.org/en/download/).

The project includes [React](https://facebook.github.io/react/) and [ReactDOM](https://facebook.github.io/react/docs/react-dom.html) as dependencies, as well as [(D3) Data-Driven Documents](https://d3js.org/) and other open-source libraries. <br>
 - Run `npm install --save` to install all dependencies exclusively for this project.


### Initialization & Setup
Clone the project to your computer. Then, **in the project directory**, you may run any of the following modes to view the app:

##### Development Mode:
- `npm start` runs the app in development mode.
- Open [(localhost:3000)](http://localhost:3000) to view it in the browser.


##### Production Mode:
- `npm run build` builds the app for production and optimizes the build for the best performance.
- `npm install -g serve` to build a static server.
- `serve -s build` runs the app in production mode.
- Open [(localhost:5000)](http://localhost:5000/) to view it in the browser.


## License
Licensed under the MIT License.
