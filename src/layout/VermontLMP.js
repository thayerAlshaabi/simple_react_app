
import React, { Component } from 'react';
import * as d3 from 'd3';
import '../css/Graphs.css';

class VermontLMP extends Component {

    /*
        Original implementation source:
        MIT License Copyright (c) 2017 Artyom Trityak
        link: https://github.com/artyomtrityak/d3-explorer
    */

    constructor(props) {
    super(props);
    const parseTime = d3.timeParse("%m/%d/%Y %H:%M");
    this.state = {
        // data from ISO new England offical website 
      data: [
          {
            BeginDate: parseTime('05/11/2017 00:00'),
            ID: 4003,
            Energy: 19.95,
            LMP: 19.39,
            Loss: -0.56,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 00:30'),
            ID: 4003,
            Energy: 24.14,
            LMP: 23.42,
            Loss: -0.72,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 02:00'),
            ID: 4003,
            Energy: 24.5,
            LMP: 23.95,
            Loss: -0.55,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 02:30'),
            ID: 4003,
            Energy: 24.27,
            LMP: 23.75,
            Loss: -0.52,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 03:00'),
            ID: 4003,
            Energy: 19.56,
            LMP: 19.04,
            Loss: -0.52,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 03:30'),
            ID: 4003,
            Energy: 0,
            LMP: 0,
            Loss: 0,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 04:00'),
            ID: 4003,
            Energy: 19.69,
            LMP: 19.23,
            Loss: -0.46,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 04:30'),
            ID: 4003,
            Energy: 20.15,
            LMP: 19.73,
            Loss: -0.42,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 05:00'),
            ID: 4003,
            Energy: 19.62,
            LMP: 19.23,
            Loss: -0.39,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 05:30'),
            ID: 4003,
            Energy: 19.83,
            LMP: 19.42,
            Loss: -0.41,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 06:00'),
            ID: 4003,
            Energy: 22.37,
            LMP: 21.87,
            Loss: -0.5,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 06:30'),
            ID: 4003,
            Energy: 23.22,
            LMP: 22.77,
            Loss: -0.45,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 07:00'),
            ID: 4003,
            Energy: 24.39,
            LMP: 23.87,
            Loss: -0.52,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 07:30'),
            ID: 4003,
            Energy: 28.1,
            LMP: 27.46,
            Loss: -0.64,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 08:00'),
            ID: 4003,
            Energy: 35.36,
            LMP: 34.56,
            Loss: -0.8,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 08:30'),
            ID: 4003,
            Energy: 25.19,
            LMP: 24.68,
            Loss: -0.51,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 09:00'),
            ID: 4003,
            Energy: 25.72,
            LMP: 25.21,
            Loss: -0.51,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 09:30'),
            ID: 4003,
            Energy: 26.67,
            LMP: 26.12,
            Loss: -0.55,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 10:00'),
            ID: 4003,
            Energy: 27.29,
            LMP: 26.76,
            Loss: -0.53,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 10:30'),
            ID: 4003,
            Energy: 29.96,
            LMP: 29.29,
            Loss: -0.67,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 11:00'),
            ID: 4003,
            Energy: 35.2,
            LMP: 34.43,
            Loss: -0.77,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 11:30'),
            ID: 4003,
            Energy: 26.41,
            LMP: 25.79,
            Loss: -0.62,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 12:00'),
            ID: 4003,
            Energy: 25.9,
            LMP: 25.19,
            Loss: -0.71,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 12:30'),
            ID: 4003,
            Energy: 32.94,
            LMP: 32.06,
            Loss: -0.88,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 13:00'),
            ID: 4003,
            Energy: 25.89,
            LMP: 25.24,
            Loss: -0.65,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 13:30'),
            ID: 4003,
            Energy: 25.86,
            LMP: 25.3,
            Loss: -0.56,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 14:00'),
            ID: 4003,
            Energy: 23.8,
            LMP: 23.27,
            Loss: -0.53,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 14:30'),
            ID: 4003,
            Energy: 22.04,
            LMP: 21.55,
            Loss: -0.49,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 15:00'),
            ID: 4003,
            Energy: 23.15,
            LMP: 22.66,
            Loss: -0.49,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 15:30'),
            ID: 4003,
            Energy: 22.28,
            LMP: 21.86,
            Loss: -0.42,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 16:00'),
            ID: 4003,
            Energy: 23.33,
            LMP: 22.86,
            Loss: -0.47,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 16:30'),
            ID: 4003,
            Energy: 33.86,
            LMP: 32.97,
            Loss: -0.89,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 17:00'),
            ID: 4003,
            Energy: 40.1,
            LMP: 39.19,
            Loss: -0.91,
            Congestion: 0
        },
          {
            BeginDate: parseTime('05/11/2017 17:30'),
            ID: 4003,
            Energy: 26.69,
            LMP: 26.05,
            Loss: -0.64,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 18:00'),
            ID: 4003,
            Energy: 36.66,
            LMP: 35.77,
            Loss: -0.89,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 18:30'),
            ID: 4003,
            Energy: 25.89,
            LMP: 25.12,
            Loss: -0.77,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 19:00'),
            ID: 4003,
            Energy: 25.32,
            LMP: 24.6,
            Loss: -0.72,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 19:30'),
            ID: 4003,
            Energy: 25.18,
            LMP: 24.57,
            Loss: -0.61,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 20:00'),
            ID: 4003,
            Energy: 27.34,
            LMP: 26.76,
            Loss: -0.58,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 20:30'),
            ID: 4003,
            Energy: 27.23,
            LMP: 26.57,
            Loss: -0.66,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 21:00'),
            ID: 4003,
            Energy: 27.28,
            LMP: 26.64,
            Loss: -0.64,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 21:30'),
            ID: 4003,
            Energy: 25.23,
            LMP: 24.66,
            Loss: -0.57,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 22:00'),
            ID: 4003,
            Energy: 25.1,
            LMP: 24.53,
            Loss: -0.57,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 22:30'),
            ID: 4003,
            Energy: 20.93,
            LMP: 20.37,
            Loss: -0.56,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 23:00'),
            ID: 4003,
            Energy: 19.36,
            LMP: 18.76,
            Loss: -0.6,
            Congestion: 0
          },
          {
            BeginDate: parseTime('05/11/2017 23:30'),
            ID: 4003,
            Energy: 20.23,
            LMP: 19.46,
            Loss: -0.77,
            Congestion: 0
          }
        ]
    };
  }


  componentDidMount() {
      const width = 1000,
        height = 450;

       const x = d3.scaleTime()
         .domain([new Date(2017, 4, 11, 0, 0), new Date(2017, 4, 11, 23, 30)]) // min max dates
         .range([0, width]);

       const y = d3.scaleLinear()
         .domain([0, 60]) //max value
         .range([height, 0]);

       const lineGenerator = d3.line()
            .x(d => x(d.BeginDate))
            .y(d => y(d.LMP));

       const chart = d3.select(this.chartRef)
         .attr('width', width)
         .attr('height', height + 30)
         .append('g')
           .attr('transform', 'translate(100, 0)');

        chart.append('path')
               .attr('class', 'line')
               .style('stroke', 'red')
               .style('fill', 'none')
               .attr('d', lineGenerator(this.state.data));

        chart.append("g")
              .attr("class", "axis axis--x")
              .attr("transform", `translate(0,${y(0)})`)
              .call(d3.axisBottom(x));


        chart.append("g")
          .attr("class", "axis axis--y")
          .attr("transform", `translate(0,0)`)
          .call(d3.axisLeft(y));


        chart.append("g")
            .call(d3.axisLeft(y))
            .append("text")
            .attr("fill", "#000")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .text("PRICE ($)")
            .style('font-size', '20px');


        const legendContainer = chart
          .append('g')
            .attr('class', 'legend')
            .attr("transform", `translate(0,${y(0)+20})`);

        legendContainer.selectAll('rect')
          .enter()
            .append('rect')
              .attr('width', 0)
              .attr('height', 0)
              .attr('x', (d, i) => {
                return i * 200;
            });
}

    render() {
      return (
          <section id="VermontLMP" className="lmp-section hideAnimation">
              <div className="container">
                  <center className="row">
                      <div className="card">
                          <div className="card-block">
                              <h2 className="card-title"><i className="fa fa-line-chart fa-lg aria-hidden=true"></i> VERMONT ELECTRICITY PRICE (LMP)</h2>
                              <p className="card-text">
                                  This graph visualizes a time-series of electricity prices in Vermont (05/11/2017). Electricity prices are typically expressed in $/MWh. The locational prices are known as Locational Marginal Prices or LMPs.
                                  <a href="https://www.iso-ne.com/isoexpress/web/charts/guest-hub?p_p_id=fiveminuterealtimelmp_WAR_isoneportlet_INSTANCE_a3zQ&p_p_lifecycle=0&p_p_state=normal&p_p_state_rcv=1"> <i className="fa fa-lg fa-arrow-circle-right aria-hidden=true"> Learn more</i></a>
                              </p>
                              <svg className="card-img-top img-fluid img-responsive" ref={(r) => this.chartRef = r}></svg>
                          </div>
                      </div>
                  </center>
              </div>
              <a className="page-scroll" href="#About">
                  <i className="fa fa-4x fa-chevron-circle-down" aria-hidden="true"></i>
              </a>
          </section>
      );
    }
}

export default VermontLMP;
