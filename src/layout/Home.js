import React, { Component } from 'react';
import '../css/Home.css';

class Home extends Component {
  render() {
    return (
        <section id="Home" className="intro-section hideAnimation">
            <div className="container">
                <div className="row">
                    <div>
                      <center><div className="Home-header">
                        <img src="http://static1.squarespace.com/static/57ee54786a4963a32de6863b/t/57fe21dbff7c50274b156d17/1494336061563/?format=1500w" className="Home-logo img-fluid img-responsive" alt="logo" />
                        <br />
                      </div>
                      <blockquote className="blockquote">
                        <h1>THE FUTURE OF SMART ENERGY</h1>
                        <p className="Home-intro">
                            Packetized Energy designs and deploys human-friendly systems to enable distributed energy resources such as water heaters,
                            electric vehicle chargers, battery storage systems and pool pumps to balance supply and demand in the power grid.
                            As a result, people can better manage their energy costs and the grid can run reliably with renewable energy.
                        </p>
                        <footer className="blockquote-footer"> Packetized Energy Technologies, Inc.</footer>
                      </blockquote></center>
                    </div>
                </div>
            </div>
            <a className="page-scroll" href="#FuelMix">
                <i className="fa fa-4x fa-chevron-circle-down activeAnimation" aria-hidden="true"></i>
            </a>
        </section>
    );
  }
}

export default Home;
