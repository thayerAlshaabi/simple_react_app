import React, { Component } from 'react';
import '../css/Navigation.css';

class Navigation extends Component {
  render() {
    return (
      <div className="Navigation">
          <nav className="navbar navbar-default navbar-fixed-top" role="navigation">
              <div className="container">
                  <div className="navbar-header page-scroll">
                      <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                      </button>
                      <a className="navbar-brand page-scroll" href="#page-top">
                          <img src="http://static1.squarespace.com/static/57ee54786a4963a32de6863b/t/57fe21dbff7c50274b156d17/1494336061563/?format=1500w" className="brand img-responsive" alt="logo" />
                      </a>
                  </div>
                  <div className="collapse navbar-collapse" id="myNavbar">
                      <ul className="nav navbar-nav navbar-right">
                          <li className="activeAnimation"><a className="page-scroll" href="#page-top">Home</a></li>
                          <li><a className="page-scroll" href="#FuelMix">FuelMix</a></li>
                          <li><a className="page-scroll" href="#VermontLMP">LMP</a></li>
                          <li><a className="page-scroll" href="#About">About</a></li>
                      </ul>
                  </div>
              </div>
          </nav>
      </div>
    );
  }
}

export default Navigation;
